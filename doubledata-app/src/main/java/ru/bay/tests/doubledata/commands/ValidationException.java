package ru.bay.tests.doubledata.commands;

/**
 * Ошибка проверки корректности сигнатуры комманды
 * @author Dmitriy
 */
public class ValidationException extends Exception {

	private static final long serialVersionUID = -6446958342759747001L;
	
	public ValidationException(String message) {
		super(message);
	}
}
