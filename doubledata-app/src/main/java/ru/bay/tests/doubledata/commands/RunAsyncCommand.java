package ru.bay.tests.doubledata.commands;

import ru.bay.tests.doubledata.Job;

/**
 * Комманда READ-ASYNC
 * @author Dmitriy
 */
public class RunAsyncCommand extends RunCommand {
	public static final String NAME = "RUN-ASYNC";
	
	public RunAsyncCommand(Job job, String[] args) {
		super(job, args);
	}

	@Override
	public void execute() {
		job.runAsync(file, timeout);
	}
}
