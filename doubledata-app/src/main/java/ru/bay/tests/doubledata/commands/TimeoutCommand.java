package ru.bay.tests.doubledata.commands;

import ru.bay.tests.doubledata.Job;

/**
 * Комманда TIMEOUT
 * @author Dmitriy
 */
public class TimeoutCommand extends Command {
	public static final String NAME = "TIMEOUT";
	
	private long timeout;
	
	public TimeoutCommand(Job job, String[] args) {
		super(job, args);
	}

	@Override
	public void execute() {
		job.timeout(timeout);
	}

	@Override
	public void validateArgs() throws ValidationException {
		if (args == null || args.length != 1) {
			new ValidationException("One argument expected");
		}
		try {
			Long t = Long.parseLong(args[0]);
			timeout = t;
		} catch(NumberFormatException e) {
			new ValidationException("Incorrect timeout value");
		}
	}

}
