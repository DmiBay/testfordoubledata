package ru.bay.tests.doubledata.commands;

import ru.bay.tests.doubledata.Job;

/**
 * Комманда STOP
 * @author Dmitriy
 */
public class StopCommand extends Command {
	public static final String NAME = "STOP";
	
	public StopCommand(Job job, String[] args) {
		super(job, args);
	}

	@Override
	public void execute() {
		job.stop();
	}

	@Override
	public void validateArgs() throws ValidationException {
		if (args != null && args.length != 0) {
			new ValidationException("No arguments expected");
		}
	}

}
