package ru.bay.tests.doubledata.commands;

import ru.bay.tests.doubledata.Job;

/**
 * Родительский класс для комманд запуска параллельного выполнения задач
 * @author Dmitriy
 */
public abstract class RunCommand extends Command {

	protected String file;
	protected long timeout;
	
	protected RunCommand(Job job, String[] args) {
		super(job, args);
	}

	@Override
	public void validateArgs() throws ValidationException {
		if (args == null || args.length != 2) {
			new ValidationException("Two arguments expected");
		}
		try {
			Long t = Long.parseLong(args[1]);
			file = args[0];
			timeout = t;
		} catch(NumberFormatException e) {
			new ValidationException("Incorrect timeout value");
		}
	}

}
