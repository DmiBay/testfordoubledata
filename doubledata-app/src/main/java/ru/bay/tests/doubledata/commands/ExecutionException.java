package ru.bay.tests.doubledata.commands;

/**
 * Обобщение для ошибок, которые могут возникнуть в ходе выполнения комманд
 * @author Dmitriy
 */
public class ExecutionException extends Exception {

	private static final long serialVersionUID = 1465150294001068255L;

}
