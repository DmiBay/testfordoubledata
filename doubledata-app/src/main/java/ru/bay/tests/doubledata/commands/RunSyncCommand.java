package ru.bay.tests.doubledata.commands;

import ru.bay.tests.doubledata.Job;

/**
 * Комманда READ-SYNC
 * @author Dmitriy
 */
public class RunSyncCommand extends RunCommand {
	public static final String NAME = "RUN-SYNC";
		
	public RunSyncCommand(Job job, String[] args) {
		super(job, args);
	}

	@Override
	public void execute() {
		job.runSync(file, timeout);
	}

}
