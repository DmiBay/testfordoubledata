package ru.bay.tests.doubledata;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Job executor starter
 * Expected one argument, it should be a path to main job file
 * @author Dmitriy
 */
public class Application {

	public static void main(String[] args) {
		Path path = Paths.get(args[0]);
		
		if (!path.toFile().exists()) {
			System.err.println(String.format("Path '%s' does not exist", path));
		}
		if (path.toFile().isDirectory()) {
			System.err.println(String.format("Path '%s' is not a file", path));	
		}
		
		try {
			JobExecutor jobExecutor = new JobExecutor(path);
			jobExecutor.start();
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
	}
}
