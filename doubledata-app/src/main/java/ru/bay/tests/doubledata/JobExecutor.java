package ru.bay.tests.doubledata;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Исполнитель задач
 * @author Dmitriy
 */
public class JobExecutor {

	private final ScheduledThreadPoolExecutor executor;
	private final Path jobFile;
	private final Context context;
	private Future<List<String>> future;
	
	/**
	 * Создание исполнителя для задачи, описанной в файле
	 * @param file путь до файла главной задачи
	 */
	public JobExecutor(Path file) {
		this.executor = new ScheduledThreadPoolExecutor(10);
		this.executor.setKeepAliveTime(10L, TimeUnit.SECONDS);
		this.executor.setRemoveOnCancelPolicy(true);
		
		this.jobFile = file;
		this.context = new Context(file.getParent(), this);
	}
	
	/**
	 * Размещение задачи в очереди на исполнение, 
	 * задача при этом будет исполняться до самостоятельного завершения
	 * @param job
	 * @return
	 */
	public Future<?> execute(Job job){
		Future<List<String>> future = executor.submit(job);
		return future;
	}
	
	/**
	 * Размещение задачи в очереди на исполнение с указанием времени жизни задачи
	 * задача при этом будет исполняться до самостоятельного завершения, 
	 * но будет остановлена принудительно, если время выполнения превышает время жизни
	 * @param job
	 * @param msec
	 * @return
	 */
	public Future<?> execute(Job job, long msec){
		Future<?> future = execute(job);
		Terminator terminator = new Terminator(future);
		executor.schedule(terminator, msec, TimeUnit.MILLISECONDS);
		return future;
	}
	
	/**
	 * Запуск исполнителя задач, стартует исполнение главной задачи
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	public void start() throws IOException {
		Job mainJob = new Job(jobFile, context); 
		future = (Future<List<String>>) execute(mainJob);
	}
	
	/**
	 * Получение результатов выполнения главной задачи
	 * @return
	 */
	public List<String> getResult() {
		List<String> result = new ArrayList<>();
		if (future == null) {
			return result;
		}
		try {
			result = future.get();
			System.out.println(String.format("Result: %s", result));
			return result;
		} catch (InterruptedException | ExecutionException e) {
			return result;
		}
	}
	
	/**
	 * Контекст исполнения задач, предоставляет задачам доступ к исполнителю
	 * и возможность получать файлы из директории главной задачи
	 * @author Dmitriy
	 */
	public class Context {
		private Path jobDirectory;
		private JobExecutor jobExecutor;
		
		public Context(Path root, JobExecutor jobExecutor) {
			this.jobDirectory = root;
			this.jobExecutor = jobExecutor;
		}

		public JobExecutor getJobExecutor() {
			return jobExecutor;
		}
		
		public Path getFile(String name) {
			return jobDirectory.resolve(name);
		}
	}
	
	/**
	 * Выключатель для принудительной остановки задачи
	 * @author Dmitriy
	 */
	private class Terminator implements Runnable{
		private Future<?> victim;
		
		public Terminator(Future<?> victim) {
			this.victim = victim;
		}

		@Override
		public void run() {
			victim.cancel(true);
		}
	}
}
