package ru.bay.tests.doubledata;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import ru.bay.tests.doubledata.JobExecutor.Context;
import ru.bay.tests.doubledata.commands.Command;
import ru.bay.tests.doubledata.commands.ExecutionException;
import ru.bay.tests.doubledata.commands.PushCommand;
import ru.bay.tests.doubledata.commands.ReadCommand;
import ru.bay.tests.doubledata.commands.RunAsyncCommand;
import ru.bay.tests.doubledata.commands.RunSyncCommand;
import ru.bay.tests.doubledata.commands.StopCommand;
import ru.bay.tests.doubledata.commands.TimeoutCommand;
import ru.bay.tests.doubledata.commands.ValidationException;

/**
 * Класс задачи
 * @author Dmitriy
 */
public class Job implements Callable<List<String>> {

	private final String name;
	private final List<String> instructions;
	private final List<String> buffer;
	private final Context context;
 	
	public Job(Path file, Context context) throws IOException {
		this(file, context, Collections.synchronizedList(new LinkedList<String>()));
	}

	public Job(Path file, Context context, List<String> buffer) throws IOException {
		this.buffer = buffer;
		this.context = context;
		this.name = file.getFileName().toString();
		this.instructions = Files.readAllLines(file);
	}
	
	
	@Override
	public List<String> call() throws Exception {
		try {
			instructions.forEach(instr -> processInstruction(instr));
		} catch (BreakException e) {
			//Noop, just stop the job
		}
		return buffer;
	}
	
	/**
	 * Обработка одной строки из файла задачи
	 * @param instruction
	 */
	private void processInstruction(String instruction) {
		try {
			Command command = parseCommand(instruction);
			System.out.println(String.format("%d: %s -> %s", 
					System.currentTimeMillis(), this.getName(), instruction));
			command.execute();
		} catch (ValidationException | ExecutionException e) {
			throw new BreakException();
		}
	}

	/**
	 * Определение команды и её параметров
	 * @param instruction
	 * @return объект исполняемой команды
	 * @throws ValidationException если обнаружена неизвестная команда, либо некорректный для команды формат
	 */
	private Command parseCommand(String instruction) throws ValidationException {
		String whitespaces = "\\s+";
		String[] words = instruction.split(whitespaces, 2);
		String[] args = (words.length > 1) ? words[1].split(whitespaces) : null;
		
		Command command;
		if (StopCommand.NAME.equals(words[0])) {
			command = new StopCommand(this, args);
		} else if (TimeoutCommand.NAME.equals(words[0])) {
			command = new TimeoutCommand(this, args);
		} else if (PushCommand.NAME.equals(words[0])) {
			command = new PushCommand(this, args);
		} else if (ReadCommand.NAME.equals(words[0])) {
			command = new ReadCommand(this, args);
		} else if (RunAsyncCommand.NAME.equals(words[0])) {
			command = new RunAsyncCommand(this, args);
		} else if (RunSyncCommand.NAME.equals(words[0])) {
			command = new RunSyncCommand(this, args);
		} else {
			throw new ValidationException("Unknown command");
		}
		command.validateArgs();
		
		return command;
	}

	/**
	 * Остановка выполнения задачи
	 * Действие команды STOP
	 */
	public void stop() {
		throw new BreakException();
	}
	
	/**
	 * Действие команды TIMEOUT
	 * @param msec
	 */
	public void timeout(long msec) {
		try {
			Thread.sleep(msec);
		} catch (InterruptedException e) {
			stop();
		}
	}
	
	/**
	 * Действие команды READ
	 * @param fileName
	 */
	public void read(String fileName) {
		Path file = checkAndGetFile(fileName);
		try {
			List<String> lines = Files.readAllLines(file);
			push(String.join("\n", lines));
		} catch (IOException e) {
			stop();
		}
	}

	/**
	 * Действие команды RUN-ASYNC
	 * @param fileName
	 * @param msec
	 */
	public void runAsync(String fileName, long msec) {
		Path file = checkAndGetFile(fileName);
		try {
			Job job = new Job(file, context, buffer);
			context.getJobExecutor().execute(job, msec);
		} catch (IOException e) {
			stop();
		}
	}
	
	/**
	 * Действие команды RUN-SYNC
	 * @param fileName
	 * @param msec
	 */
	public void runSync(String fileName, long msec) {
		Path file = checkAndGetFile(fileName);
		Future<?> future = null;
		try {
			Job job = new Job(file, context, buffer);
			future = context.getJobExecutor().execute(job);
			future.get(msec, TimeUnit.MILLISECONDS);
		} catch (TimeoutException e) {
			//cancel subjob
			future.cancel(true);
		} catch (InterruptedException | IOException e) {
			stop();
		} catch (java.util.concurrent.ExecutionException e) {
			//go on
		}
	}
	
	/**
	 * Проверка существования файла с указанным именнем
	 * и его получение
	 * @param fileName
	 * @return
	 */
	private Path checkAndGetFile(String fileName) {
		Path file = context.getFile(fileName);
		if (!file.toFile().exists()) {
			stop();
		}
		return file;
	}
	
	/**
	 * Действие команды PUSH
	 * @param message
	 */
	public void push(String message) {
		buffer.add(message);
	}
	
	/**
	 * Получение буффера результатов задачи
	 * @return
	 */
	public List<String> getBuffer() {
		return buffer;
	}
	
	/**
	 * Имя задачи, определяется по имени исполняемого файла
	 * @return
	 */
	public String getName() {
		return name;
	}
}
