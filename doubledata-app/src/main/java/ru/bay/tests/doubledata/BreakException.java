package ru.bay.tests.doubledata;

/**
 * BreakException is needed to terminate job execution itself
 * @author Dmitriy
 */
public class BreakException extends RuntimeException {

	private static final long serialVersionUID = 721553262369272936L;
}
