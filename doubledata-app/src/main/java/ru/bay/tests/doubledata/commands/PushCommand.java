package ru.bay.tests.doubledata.commands;

import ru.bay.tests.doubledata.Job;

/**
 * Комманда PUSH
 * @author Dmitriy
 */
public class PushCommand extends Command {
	public static final String NAME = "PUSH";
	
	private String message;
	
	public PushCommand(Job job, String[] args) {
		super(job, args);
	}

	@Override
	public void execute() {
		job.push(message);
	}

	@Override
	public void validateArgs() throws ValidationException {
		if (args == null || args.length != 1) {
			new ValidationException("Only one argument expected");
		}
		message = args[0];
	}

}
