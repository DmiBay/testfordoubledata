package ru.bay.tests.doubledata.commands;

import ru.bay.tests.doubledata.Job;

/**
 * Комманда READ
 * @author Dmitriy
 */
public class ReadCommand extends Command{
	public static final String NAME = "READ";
	
	private String file;
	
	public ReadCommand(Job job, String[] args) {
		super(job, args);
	}

	@Override
	public void execute() {
		job.read(file);
	}

	@Override
	public void validateArgs() throws ValidationException {
		if (args == null || args.length != 1) {
			new ValidationException("Only one argument expected");
		}
		file = args[0];
	}

}
