package ru.bay.tests.doubledata.commands;

import ru.bay.tests.doubledata.Job;

/**
 * Родительский класс для всех типов комманд
 * @author Dmitriy
 */
public abstract class Command {
	
	protected Job job;
	protected String[] args;
	
	protected Command(Job job, String[] args) {
		this.job = job;
		this.args = args;
	}	
	
	/**
	 * Выполнение действия комманды
	 * @throws ExecutionException
	 */
	public abstract void execute() throws ExecutionException;
	
	/**
	 * Валидация аргументов комманды
	 * без выполнения валидации выполнение комманды будет некорректным
	 * @throws ValidationException
	 */
	public abstract void validateArgs() throws ValidationException;
	
}
