package ru.bay.tests.doubledata;

import static org.junit.Assert.*;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.junit.Before;
import org.junit.Test;


public class JobExecutionTest {

	private Path jobFile;
	
	@Before
	public void init() {
		URL url = getClass().getResource("/job1.txt");
		try {
			jobFile = Paths.get(url.toURI());
		} catch (URISyntaxException e) {
		}
	}
	
	@Test
	public void testJobFile() {
		assertNotNull("Test job file is not defined", jobFile);
	}
	
	@Test
	public void testExecution() {
		JobExecutor executor = new JobExecutor(jobFile);
		try {
			executor.start();
			List<String> result = executor.getResult();
			assertEquals("Unexpected result buffer size", 4, result.size());
		} catch (IOException e) {
			assertTrue(e.getMessage(), false);
		}
	}
}
